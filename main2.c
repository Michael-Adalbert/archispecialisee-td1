#include <ammintrin.h>
#include <stdio.h>
#define N 32
int main()
{

    char a[N];
    __m128i tmp, b;

    tmp = _mm_set_epi8(15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0);
    b = _mm_set1_epi8(16);
    for (int i = 0; i < N; i += 16)
    {
        _mm_store_si128((__m128i *)&a[i], tmp);
        tmp = _mm_add_epi8(tmp, b);
    }

    for (int i = 0; i < N; i++)
        printf("%d ", a[i]);
    printf("\n");

    return 0;
}