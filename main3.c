#include <ammintrin.h>
#include <stdio.h>
#define N 8

int main()
{
    int a[N] = {1, 2, 3, 4, 1, 2, 3, 4};
    int res[4] = {0, 0, 0, 0};
    __m128i ax, bx;
    bx = _mm_load_si128((__m128i *)res);
    for (int i = 0; i < N; i += 4)
    {
        ax = _mm_load_si128((__m128i *)&a[i]);
        bx = _mm_add_epi32(bx, ax);
    }
    _mm_store_si128((__m128i *)res, bx);
    int x = res[0] + res[1] + res[2] + res[3];

    printf("%d \n", x);

    return 0;
}