#include <ammintrin.h>
#include <stdio.h>

#define N 24

void fill(double *t, int size)
{
    for (int i = 0; i < size; i++)
        t[i] = i;
}

int main()
{

    double a[N], b[N], c[N];
    fill(b, N);
    fill(c, N);

    __m128d ax, bx, cx;
    for (int i = 0; i < N; i += 2)
    {
        bx = _mm_load_pd(&b[i]);
        cx = _mm_load_pd(&c[i]);
        ax = _mm_add_pd(bx, cx);
        _mm_store_pd(&a[i], ax);
    }

    for (int i = 0; i < N; i++)
        printf("%f ", a[i]);
    printf("\n");

    return 0;
}